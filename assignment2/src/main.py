import cv2
import os
import time
import json
root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

allimages = []
trutharray = []
outarrayface = []
outarrayeye = []
outarrayprofileface = []
numOfImages = 0

class imageEntry:
    imagepath = ""
    faces = 0
    time = 0

    def __init__(self, imagepath, faces, time):
        self.imagepath = imagepath
        self.faces = faces
        self.time = time

    def to_dict(self):
        serialized = {}
        serialized['imagepath'] = self.imagepath
        serialized['faces'] = self.faces
        serialized['time'] = self.time
        return serialized

for (dirpath, dirnames, filenames) in os.walk(root+"\\dataset"):
    allimages = filenames
    numOfImages = len(filenames)

def haarcascadeface(path):
    print(path +": Working on: Haar cascade - default face")
    start = time.time()
    cascPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)
    gray = cv2.imread(path, 0)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    # plt.show()  # Show pre-processed

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 255, 255), 3)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    # plt.show()
    
    end = time.time()
    elapsed = end - start
    temp = imageEntry(root+"\\dataset\\"+singleImagePath, len(faces), round(elapsed,2))

    return temp

def haarcascadeprofileface(path):
    print(path +": Working on: Haar cascade - side face")
    start = time.time()
    profilefacecascPath = "haarcascade_profileface.xml"
    profilefaceCascade = cv2.CascadeClassifier(profilefacecascPath)
    gray = cv2.imread(path, 0)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    # plt.show()

    faces = profilefaceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    for (x, y, w, h) in faces:
        cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 255, 255), 3)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    # plt.show()
    
    end = time.time()
    elapsed = end - start
    temp = imageEntry(root+"\\dataset\\"+singleImagePath,len(faces), round(elapsed,2))

    return temp

def haarcascadeeyes(path):
    print(path +": Working on: Haar cascade - eyes")
    start = time.time()

    eyePath = "haarcascade_eye.xml"
    eyeCascade = cv2.CascadeClassifier(eyePath)
    gray = cv2.imread(path, 0)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    # plt.show()

    faces = eyeCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    
    for (x, y, w, h) in faces:
        cv2.rectangle(gray, (x, y), (x + w, y + h), (255, 255, 255), 3)
    # plt.figure(figsize=(12, 8))
    # plt.imshow(gray, cmap='gray')
    
    end = time.time()
    elapsed = end - start
    temp = imageEntry(path, len(faces), round(elapsed,2))
    # plt.show()

    return temp

with open('groundtruth.json', 'r') as infile:
    data = json.loads(infile.read())
    for i in range(0,numOfImages):
        temp = imageEntry(data[i]['imagepath'], data[i]['faces'], data[i]['time'])
        trutharray.append(temp)

print("Ground truth: ")
for element in trutharray:
    print(element.imagepath, element.time, element.faces)

for singleImagePath in allimages:

    temp = haarcascadeface(root+"\\dataset\\"+singleImagePath)
    outarrayface.append(temp)

    temp = haarcascadeprofileface(root + "\\dataset\\" + singleImagePath)
    outarrayprofileface.append(temp)

    temp = haarcascadeeyes(root + "\\dataset\\" + singleImagePath)
    outarrayeye.append(temp)

print("---")
print("Faces detected:")
print("Truth:")
for element in trutharray:
    # jsonoutarray.append(element.to_dict())
    # print(element.imagepath, element.time, element.faces)
    print(element.faces, end=',')
print("")

print("Haar cascade - face detector:")
for element in outarrayface:
    # jsonoutarray.append(element.to_dict())
    # print(element.imagepath, element.time, element.faces)
    print(element.faces, end=',')
print("")

print("Haar cascade - profile face detector:")
for element in outarrayprofileface:
    # print(element.imagepath, element.time, element.faces)
    print(element.faces, end = ',')
print("")

print("Haar cascade - eye detector:")
for element in outarrayeye:
    # print(element.imagepath, element.time, element.faces)
    print(element.faces, end=',')
print("")

print("Haar cascade - average combination:")
for i in range (0, numOfImages):
    temp = (outarrayprofileface[i].faces+outarrayface[i].faces+(outarrayeye[i].faces/2))/3
    print(round(temp,2), end=',')
print("")
print("Haar cascade - xor combination:")
for i in range (0, numOfImages):
    a = 0
    if outarrayprofileface[i].faces > 0:
        a = a+1
    if outarrayface[i].faces > 0:
        a = a+1
    if outarrayeye[i].faces > 0:
        a = a+1

    print(round(a/3, 2), end=',')
print("")